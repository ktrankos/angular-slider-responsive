angular.module('website', ['ngAnimate', 'ngTouch', 'ngSanitize'])
    .controller('MainCtrl', function ($scope, $element) {
        
        $scope.items = 
        [
            {img: 'img-00', text: "<h1>HOLA A TODOS soy uno</h1>"},
            {img: 'img-01', text: "<h1>HOLA A TODOS soy dos</h1>"},
            {img: 'img-02', text: "<h1>HOLA A TODOS soy tres</h1>"},
            {img: 'img-03', text: "<h1>HOLA A TODOS soy cuatro</h1>"},
            {img: 'img-04', text: "<h1>HOLA A TODOS soy cu34434</h1>"},
            {img: 'img-05', text: "<h1>HOLA A TODOS soy la la la</h1>"}


        ];


        $scope.direction = 'left';
        $scope.currentIndex = 0;
        

        var $el = $($element);
        //console.log($el.find('.row').height(), $el, $el.find('.row'));
        $scope.topArrow = $el.find('.row img').height()/2-35;
        //$scope.heightCont  = $el.find('.row img').height();
        $(window).resize(function(){
            $scope.topArrow = $el.find('.row img').height()/2-35;
            //$scope.heightCont  = $el.find('.row img').height();

        });
        $scope.$watch('topArrow', function(){
            $($element).find('.arrow').css({
                top: $scope.topArrow
            });
            //$($element).find('.int-wrapper').height($scope.heightCont);
        });
        $scope.setCurrentSlideIndex = function (index) 
        {
            $scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
            $scope.currentIndex = index;
        };
        $scope.isCurrentSlideIndex = function (index) {
            return $scope.currentIndex === index;
        };
        $scope.prevSlide = function ($e) 
        {
            $scope.direction = 'left';
            $scope.currentIndex = ($scope.currentIndex <  2) ? ++$scope.currentIndex : 0;
        };
        $scope.nextSlide = function ($e) 
        {            
            $scope.direction = 'right';            
            $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : 2;
        };
    })
    .animation('.slide-animation', function () {
        return {
            beforeAddClass: function (element, className, done) {
                var scope = element.scope();

                if (className == 'ng-hide') {
                    var finishPoint = element.parent().width();
                    if(scope.direction !== 'right') {
                        finishPoint = -finishPoint;
                    }
                    TweenMax.to(element, 0.5, {left: finishPoint, onComplete: done });
                }
                else {
                    done();
                }
            },
            removeClass: function (element, className, done) {
                var scope = element.scope();

                if (className == 'ng-hide') {
                    element.removeClass('ng-hide');

                    var startPoint = element.parent().width();
                    if(scope.direction === 'right') {
                        startPoint = -startPoint;
                    }

                    TweenMax.fromTo(element, 0.5, { left: startPoint }, {left: 0, onComplete: done });
                }
                else {
                    done();
                }
            }
        };
    });


